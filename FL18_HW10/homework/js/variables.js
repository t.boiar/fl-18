const playerX1 = document.getElementById('player-X1');
const playerO2 = document.getElementById('player-O2');

const scoreX1 = document.getElementById('score-X1');
const scoreO2 = document.getElementById('score-O2');

const gameResult = document.getElementById('result');

const newGame = document.getElementById('new-game');
const reset = document.getElementById('reset');

const playerX1Moves = new Set();
const playerO2Moves = new Set();
const winCombos = [
	new Set(['1', '2', '3']),
	new Set(['4', '5', '6']),
	new Set(['7', '8', '9']),
	new Set(['1', '4', '7']),
	new Set(['2', '5', '8']),
	new Set(['3', '6', '9']),
	new Set(['1', '5', '9']),
	new Set(['3', '5', '7'])
]

export {
  playerX1, playerO2, scoreX1, scoreO2, gameResult, newGame, reset, playerX1Moves, playerO2Moves, winCombos
};
