import {
  playerX1, playerO2, scoreX1, scoreO2, gameResult, newGame, reset, playerX1Moves, playerO2Moves, winCombos
} from './variables.js';

let cells = document.querySelectorAll('td');

let halfOfOne = 0.5;
let turnOfPlayerX1 = Math.random() < halfOfOne;

let gameInProgress = true;

const highlightPlayer = () => {
	if(turnOfPlayerX1) {
		playerX1.classList.add('highlight');
		playerO2.classList.remove('highlight');
	} else {
		playerO2.classList.add('highlight');
		playerX1.classList.remove('highlight');
	}
}

const startNewGame = () => {
  gameResult.innerHTML = '';
	cells.forEach(function(elem) {
		elem.innerHTML = '';
		elem.classList.remove('highlight');
	});
	playerX1Moves.clear();
	playerO2Moves.clear();
	gameInProgress = true;
	turnOfPlayerX1 = Math.random() < halfOfOne;
	highlightPlayer();
};
newGame.addEventListener('click', startNewGame);

const resetResult = () => {
	startNewGame();
	scoreX1.innerHTML = '0';
	scoreO2.innerHTML = '0';
};
reset.addEventListener('click', resetResult);

function intersection(setA, setB) {
	let _intersection = new Set()
	for (let elem of setB) {
			if (setA.has(elem)) {
					_intersection.add(elem);
			}
	}
	return _intersection
}

const checkWinCombo = (playerMoves) => {
	const comboLength = 3;
	for(let combo of winCombos) {
		let coincidence = intersection(combo, playerMoves);
		if(coincidence.size === comboLength) {
			return coincidence;
		}
	}
	return false;
}

const markWinner = (playerScore, combo) => {
	let player = turnOfPlayerX1 ? '1' : '2';
	gameResult.innerHTML = `🏆 Player ${player} won`;
	playerScore.innerHTML = parseInt(playerScore.innerHTML) + 1;
	for(let cell of combo) {
		document.getElementById(cell).classList.add('highlight');
	}
	gameInProgress = false;
}

highlightPlayer();
const makeMove = (event) => {
	let checkedResult = 0;
	if(!event.target.innerHTML && gameInProgress){
		if(turnOfPlayerX1) {
			event.target.innerHTML = 'X';
			playerX1Moves.add(event.target.id);
			checkedResult = checkWinCombo(playerX1Moves);
			if(checkedResult) {
				markWinner(scoreX1, checkedResult);
			}
		} else {
			event.target.innerHTML = 'O';
			playerO2Moves.add(event.target.id);
			checkedResult = checkWinCombo(playerO2Moves);
			if(checkedResult) {
				markWinner(scoreO2, checkedResult);
			}
		} 
		turnOfPlayerX1 = !turnOfPlayerX1;
		highlightPlayer();
		let fullTable = 9;
		if(playerX1Moves.size + playerO2Moves.size === fullTable){
			gameResult.innerHTML = `Draw!`;
			scoreX1.innerHTML = parseInt(scoreX1.innerHTML) + 1;
			scoreO2.innerHTML = parseInt(scoreO2.innerHTML) + 1;
		}
	}
}

cells.forEach(function(elem) {
	elem.addEventListener('click', makeMove);
});
