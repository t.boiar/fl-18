const HtmlWebpackPlugin = require('html-webpack-plugin');
const Minicss = require('mini-css-extract-plugin');
const path = require('path');

require('babel-polyfill');

module.exports = {
	target: ['web', 'es5'],
  entry: ['babel-polyfill', `./js/variables.js`, `./js/game.js`, './scss/style.scss'],
  output: {
    path: path.resolve(__dirname, `dist`),
    filename: 'bundle.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          Minicss.loader,
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(png|jpe?g|webp|git|svg|)$/i,
        use: [
          {
            loader: 'img-optimize-loader'
          }
        ]
      }

    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: 'body'
    }),
    new Minicss({
      filename: 'style.css'
    })
  ],
  devServer: {
    contentBase: './',
    port: 8000
  }
};
