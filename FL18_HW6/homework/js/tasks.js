function getMaxEvenElement(arr) {
	return Math.max(...arr.reduce((arr, item) => {
			if(item % 2 === 0) {
			arr.push(item);
		}
		return arr;
	}, []))
}

let a = 3;
let b = 5;
[a, b] = [b, a];

function getValue(value) {
	value = this.value = value ?? '-'
	return value
}

function getObjFromArray(arrayOfArrays) {
	return arrayOfArrays.reduce((arr, item) => {
		return {
			...arr,
			[item[0]]: item[1]
		};
	}, {});
}

function addUniqueId(obj1) {
	return {...obj1, id: Symbol()}
}

function getRegroupedObject(oldObj) {
	let {name, details} = oldObj;
	let {id, age, university} = details;
	return {'university':university, 'user':{ 'age': age, 'firstName': name, 'id': id}}
}

function getArrayWithUniqueEleents(arr) {
	let set = new Set(arr);
	return Array.from(set)
}

function hideNumber(phoneNumber) {
	let lastFourDigits = phoneNumber.slice(-4);
	return lastFourDigits.padStart(phoneNumber.length, '*');
}

function required(param) {
	throw new Error(`${param} is required`);
}

function add(a = required('a'), b = required('b')) {
	return a + b;
}

function* generateIterableSequence() {
	yield 'I';
	yield 'love';
	yield 'EPAM';
}