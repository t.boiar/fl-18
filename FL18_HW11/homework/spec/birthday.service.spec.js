const BirthdayService = require('../src/birthday.service');

let bs = new BirthdayService();

describe('BirthdayService', () => {
	it('Should accept new Date() in argument', () => {
		expect(() => {
			bs.howLongToMyBirthday(2009, 1, 2)
		}).toThrow('Wrong argument!');
	});
	it('Should congrats if birthday is today', () => {
		bs.howLongToMyBirthday(new Date()).then(data => {
			expect(data).toEqual('Hooray!!! It is today!');
		})
	});
	it('Should check if b-day in the next half year and notify numbers of days', () => {
		let date = new Date();
		date.setDate(date.getDate() + 160)
		bs.howLongToMyBirthday(date).then(data => {
			expect(data).toEqual(`Soon...Please, wait just 160 day/days`);
		})
	});
	it('Should check if b-day has already happened less than 6 month ago and notify numbers of days', () => {
		let date = new Date();
		date.setDate(date.getDate() - 10)
		bs.howLongToMyBirthday(date).then(data => {
			expect(data).toEqual(`Soon...Please, wait just 10 day/days`);
		})
	});
	it('Should returns the string', () => {
		bs.howLongToMyBirthday(new Date(2009, 1, 2)).then(data => {
			expect(data).toBeInstanceOf(String);
		})
	});
});
