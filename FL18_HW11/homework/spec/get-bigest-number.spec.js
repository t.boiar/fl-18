const getBigestNumber = require('../src/get-bigest-number');

describe('getBigestNumber function', () => {
	it('Should accept 2 or more arguments', () => {
		expect(() => {
			getBigestNumber(123)
		}).toThrow('Not enough arguments');
	});
	it('Should accept 10 or less arguments', () => {
		expect(() => {
			getBigestNumber(123, 16, 12, 3, 123, 12343, 6050, 123, 123, 12343, 6050, 123)
		}).toThrow('Too many arguments');
		expect(() => {
			getBigestNumber(10, 12, 14, 14, 10, 12, 14, 14, 78, 190, 123)
		}).toThrow('Too many arguments');
	});
	it('Should returns the biggest value', () => {
		expect(getBigestNumber(123, 10, 12))
		.toEqual(123);
		expect(getBigestNumber(10, 12, 3, 123, 4, 6050, 123)).toEqual(6050);
	});
	it('Each argument should be a number', () => {
		expect(() => {
			getBigestNumber(123, 10, '12')
		}).toThrow('Wrong argument type');
		expect(() => {
			getBigestNumber(10, 12, '3', 123, 12343, 6050, 123)
		}).toThrow('Wrong argument type');
	});
});