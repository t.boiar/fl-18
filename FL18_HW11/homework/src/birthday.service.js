class BirthdayService {
	constructor() {
		this.oneDay = 24 * 60 * 60 * 1000;
	}
	howLongToMyBirthday(date) {
    let today = new Date();
		if(!(date instanceof Date)) {
			throw 'Wrong argument!';
		}
		return new Promise((resolve, reject) => {
			setTimeout(() => {
			let upcomingBday = new Date(today.getFullYear(), date.getMonth(), date.getDate());

			if(today > upcomingBday) {
				upcomingBday.setFullYear(today.getFullYear() + 1);
			}
			let daysLeft = Math.ceil((upcomingBday.getTime() - today.getTime())/this.oneDay);
			if(daysLeft === 0) {
				this.congratulateWithBirthday();
			} else {
				this.notifyWaitingTime(daysLeft);
			}
			resolve(daysLeft);
			reject();
			}, 100);
			});
	}
	congratulateWithBirthday(){
		console.log('Hooray!!! It is today!')
	}
	notifyWaitingTime(waitingTime){
		if(waitingTime < 180) {
			console.log(`Soon...Please, wait just ${waitingTime} day/days`);
		} else {
			console.log(`Oh, you have celebrated it ${365 - waitingTime} day/s ago, don't you remember?`)
		}
	}
}

module.exports = BirthdayService;
