const getBigestNumber = (...args) => {
	let numbers = args;
	let minNumbers = 2;
	let maxNumbers = 10;
	if(numbers.length < minNumbers) {
		throw 'Not enough arguments';
	} else if(numbers.length > maxNumbers) {
		throw 'Too many arguments';
	}
	for(let number of numbers) {
		if(typeof number !== 'number') {
			throw 'Wrong argument type';
		}
	}
	return Math.max.apply(null, numbers);
}

module.exports = getBigestNumber;
