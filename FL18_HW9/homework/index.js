const minArticlesAmout = 5;

class Magazine {
	constructor() {
		this.state = new ReadyForPushNotification();
		this.articles = {};
		this.articleAmount = 0;
		this.subscribers = {};
		this.dafaultTimeout = 60000;
	}
	nextState() {
		this.state = this.state.next();
	}
	addArticle(article, topic) {
		if(this.articles.hasOwnProperty(topic)) {
			this.articles[topic].push(article);
		} else {
			this.articles[topic] = [article];
		}
		this.articleAmount++;
		if (this.articleAmount === minArticlesAmout) {
			this.nextState();
		}
	}
	approve(name) {
		if(this.state.approve(name)) {
			this.nextState();
		}
	}
	publish(name) {
		if(this.state.publish(name)) {
			this.nextState();
			this.notifyFollowers();
			this.returntoDefault();
			this.nextState();
		}
	}
	addSubsriber(follower, topic) {
		if(this.subscribers.hasOwnProperty(topic)) {
			this.subscribers[topic].push(follower);
		} else {
			this.subscribers[topic] = [follower];
		}
	}
	removeSubsriber(follower, topic) {
		if(this.subscribers.hasOwnProperty(topic)) {
			let followerIndex = this.subscribers[topic].indexOf(follower);
			if (followerIndex >= 0) {
				this.subscribers.splice(followerIndex, 1);
			}
		}
	}
	notifyFollowers() {
		for(let topic in this.articles) {
			if(this.subscribers.hasOwnProperty(topic)) {
				for(let follower of this.subscribers[topic]) {
					follower.onUpdate(this.articles[topic])
				}
			}
		}
	}
	returntoDefault() {
		setTimeout(function() {
			this.articles = {}
		}, this.dafaultTimeout);
	}
}

class MagazineStatus {
	constructor(statusName, nextStatus){
		this.statusName = statusName;
		this.NextStatus = nextStatus;
	}
	next(){
		return new this.NextStatus();
	}
}

class ReadyForPushNotification extends MagazineStatus {
	constructor() {
		super('readyForPushNotification', ReadyForApprove)
	}
	publish(name) {
		console.log(`Hello ${name}. You can't publish. We are creating publications now.`);
		return false;
	}
	approve(name) {
		console.log(`Hello ${name}. You can't approve. We don't have enough of publications.`);
		return false;
	}
}

class ReadyForApprove extends MagazineStatus {
	constructor() {
		super('readyForApprove', ReadyForPublish)
	}
	publish(name) {
		console.log(`Hello ${name} You can't publish. We don't have a manager's approval.`);
		return false;
	}
	approve(name) {
		console.log(`Hello ${name} You've approved the changes`);
		return true;
	}
}

class ReadyForPublish extends MagazineStatus {
	constructor() {
		super('readyForPublish', PublishInProgress)
	}
	publish(name) {
		console.log(`Hello ${name} You've recently published publications.`);
		return true;
	}
	approve(name) {
		console.log(`Hello ${name} Publications have been already approved by
		you.`);
		return false;
	}
}

class PublishInProgress extends MagazineStatus {
	constructor() {
		super('publishInProgress', ReadyForPushNotification)
	}
	publish(name) {
		console.log(`Hello ${name}. While we are publishing we can't do any actions.`);
		return false;
	}
	approve(name) {
		console.log(`Hello ${name}. While we are publishing we can't do any actions`);
		return false;
	}
}

class MagazineEmployee {
	constructor(employeeName, role, magazine) {
		this.employeeName = employeeName;
		this.role = role;
		this.magazine = magazine;
	}
	approve() {
		if (this.role === 'manager') {
			this.magazine.approve(this.employeeName);
		} else {
			console.log('you do not have permissions to do it');
		}
	}
	publish() {
		this.magazine.publish(this.employeeName);
	}
	addArticle(article) {
		if (this.role !== 'manager') {
			this.magazine.addArticle(article, this.role);
		} else {
			console.log('you do not have permissions to do it');
		}
	}
}

class Follower {
	constructor(followerName) {
		this.followerName = followerName;
	}
	subscribeTo(magazine, topic) {
		magazine.addSubsriber(this, topic);
	}
	unsubscribeTo(magazine, topic) {
		magazine.removeSubsriber(this, topic);
	}
	onUpdate(data) {
		for(let article of data) {
			console.log(`${article} ${this.followerName}`);
		}
	}
}
