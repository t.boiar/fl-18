const appRoot = document.getElementById('app-root');

/*
write your code here

list of all regions
externalService.getRegionsList();
list of all languages
externalService.getLanguagesList();
get countries list by language
externalService.getCountryListByLanguage()
get countries list by region
externalService.getCountryListByRegion()
*/

appRoot.innerHTML = `
<form id='application-form' action='#'>
<header>Countries Search</header>
<fieldset id='type-of-search'>
  <legend id='radiobutton-descr'>Please choose the type of search:</legend>
  <ul>
      <li><label for='by-region'>
        <input id='by-region' type='radio' name='Type of Search' value='By Region'
        onchange='changeRadioButton("region")'>
        By Region
      </label></li>
      <li><label for='by-language'>
        <input id='by-language' type='radio' name='Type of Search' value='By Language'
        onchange='changeRadioButton("language")'>
        By Language
      </label></li>
  </ul>
</fieldset>

<p><label for='search-query' id='dropdown-descr'>Please choose search query:</label>
    <select id='search-query' name='Search query' onchange = chooseSelectValue()>
      <option value='' selected disabled>Select value</option>
    </select></p>
</form>
    <table id='application-table'>
    <tbody id='tbody'></tbody>
    </table>
`;

const table = document.getElementById('application-table');
const tBody = document.getElementById('tbody');

let nameDirectSort = 'descending';
let areaDirectSort = null;

let typeOfSearch = 'region';
function changeRadioButton(value) {
  const noItem = document.getElementById('message');
  if(noItem){
  noItem.remove();
}
  typeOfSearch = value;

  const selectList = document.getElementById('search-query');
  let chosenList = []

  if(typeOfSearch === 'region') {
    chosenList = externalService.getRegionsList();
  } else {
    chosenList = externalService.getLanguagesList();
  }

  let length = selectList.options.length;
  for (i = length-1; i >= 0; i--) {
    selectList.options[i] = null;
  }

  for (let i = 0; i < chosenList.length; i++) {
    let option = document.createElement('option');
    option.value = chosenList[i];
    option.text = chosenList[i];
    selectList.appendChild(option);
    }
const applicationApplication = document.getElementById('application-form')
applicationApplication.insertAdjacentHTML('afterend', `<p id='message'>No items, please choose search query</p>`);
}

function chooseSelectValue(sortBy=null) {
  const noItem = document.getElementById('message');
  if(noItem){
  noItem.remove();
}
let sortedFild = 'name';
if (sortBy){
  sortedFild = sortBy;
}
  tBody.innerHTML = '';

  const trOfTableHeaders = document.createElement('tr');
  trOfTableHeaders.setAttribute('id','table-head')
  tBody.appendChild(trOfTableHeaders)

  trOfTableHeaders.innerHTML = `
  <th id='country-name' onclick='chooseSelectValue("name")'>Country name ↑</th>
  <th>Capital</th>
  <th>World region</th>
  <th>Languages</th>
  <th id='country-area' onclick='chooseSelectValue("area")'>Area ↑↓</th>
  <th>Flag</th>
  `

  const valueFromSelect = document.getElementById('search-query').value;
  let countries = [];

  if(typeOfSearch === 'region') {
    countries = externalService.getCountryListByRegion(valueFromSelect);
  } else {
    countries = externalService.getCountryListByLanguage(valueFromSelect);
  }
  let sortName = document.getElementById('country-name')
  let sortArea = document.getElementById('country-area')
  let top = -1;
  let bottom = 1;
  if(sortBy && sortBy === 'name'){
    areaDirectSort = null;
    sortArea.innerText = 'Area ↑↓'
    if(nameDirectSort && nameDirectSort === 'descending'){
      top = 1;
      bottom = -1;
      nameDirectSort = 'ascending';
      sortName.innerText = 'Country name ↓'
    } else {
      nameDirectSort = 'descending';
      top = -1;
      bottom = 1;
      sortName.innerText = 'Country name ↑'
    }
  } else if(sortBy && sortBy === 'area'){
    nameDirectSort = null;
    sortName.innerText = 'Country name ↑↓'
    if(areaDirectSort && areaDirectSort === 'descending'){
      top = 1;
      bottom = -1;
      areaDirectSort = 'ascending';
      sortArea.innerText = 'Area ↓'
    } else {
      areaDirectSort = 'descending';
      top = -1;
      bottom = 1;
      sortArea.innerText = 'Area ↑'
    }
  }

  function compare(a,b) {
    if ( a[sortedFild] < b[sortedFild] ) {
      return top;
    }
    if ( a[sortedFild] > b[sortedFild] ){
      return bottom;
    }
    return 0;
}
  countries.sort(compare);

  let rows = [];

  for(let country of countries){
    rows.push(`<tr>
      <td>${country.name}</td>
      <td>${country.capital}</td>
      <td>${country.region}</td>
      <td>${Object.values(country.languages).join(', ')}</td>
      <td>${country.area}</td>
      <td><img src='${country.flagURL}'></td>
      </tr>`)
    }
    trOfTableHeaders.insertAdjacentHTML('afterend', rows.join(' '));
  }
