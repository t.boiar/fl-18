function reverseNumber(num) {
  return (
    parseInt(
      num
        .toString()
        .split('')
        .reverse()
        .join('')
    ) * Math.sign(num)
  )
}

// console.log(reverseNumber(12345));
// console.log(reverseNumber(-56789));

function forEach(arr, func) {
  for (let i = 0; i < arr.length; i++) {
    const el = arr[i];

    func(el)
  }
}

// forEach([2,5,8], function(el) {
//   console.log(el)
// });

function map(arr, func) {
  const maped = [];

  // for (let i = 0; i < arr.length; i++) {
  //   const el = arr[i];
  //
  //   maped.push(func(el));
  // }
  forEach(arr, function(el){
    maped.push(func(el));
  })
  return maped;
}

// console.log(
// map([2, 5, 8], function(el) {
//   return el + 3;
// }));
//
// console.log(
// map([1, 2, 3, 4, 5], function(el) {
//   return el * 2;
// }));


function filter(arr, func) {
  const filteredArray = [];

  forEach(arr, function(el){
    if(func(el)) {
      filteredArray.push(el);
    }
  })

  // for (let i = 0; i < arr.length; i++) {
  //   const el = arr[i];
  //
  //   if(func(el)) {
  //     filteredArray.push(el);
  //   }
  // }
  return filteredArray;
}

// console.log(
// filter([2, 5, 1, 3, 8, 6], function(el) {
//   return el > 3
// }));
//
// console.log(
// filter([1, 4, 6, 7, 8, 10], function(el) {
//   return el % 2 === 0
// }));

function getAdultAppleLovers(data) {
  const AppleLovers =
  filter(data, function(el) {
    return el.age > 18 && el.favoriteFruit === 'apple'
  });
  const nameAppleLovers =
  map(AppleLovers, function(el) {
    return el.name
  });
  return nameAppleLovers;
  }

// console.log(getAdultAppleLovers([
// {
// "_id": "5b5e3168c6bf40f2c1235cd6",
// "index": 0,
// "age": 39,"eyeColor": "green",
// "name": "Stein",
// "favoriteFruit": "apple"
// },
// {
// "_id": "5b5e3168e328c0d72e4f27d8",
// "index": 1,
// "age": 38,
// "eyeColor": "blue",
// "name": "Cortez",
// "favoriteFruit": "strawberry"
// },
// {
// "_id": "5b5e3168cc79132b631c666a",
// "index": 2,
// "age": 2,
// "eyeColor": "blue",
// "name": "Suzette",
// "favoriteFruit": "apple"
// },
// {
// "_id": "5b5e31682093adcc6cd0dde5",
// "index": 3,
// "age": 17,
// "eyeColor": "green",
// "name": "Weiss",
// "favoriteFruit": "banana"
// }
// ]));

function getKeys(obj) {
  const keys = [];
  for (key in obj) {
    keys.push(key);
  }
return keys;
}
// console.log(getKeys({keyOne: 1, keyTwo: 2, keyThree: 3}));

function getValues(obj) {
  const values = [];

  for (key in obj) {
  values.push(obj[key])
}
return values;
}

//console.log(getValues({keyOne: 1, keyTwo: 2, keyThree: 3}));

function showFormattedDate(dateObj) {
const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  const date = dateObj.getDate()
  const month = months[dateObj.getMonth()];
  const year = dateObj.getFullYear()

return `It is ${date} of ${month}, ${year}`;
}
//console.log(showFormattedDate(new Date('2018-08-27T01:10:00')));
