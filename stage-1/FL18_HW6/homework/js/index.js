function visitLink(path) {
	if (localStorage.hasOwnProperty(path)) {
    const clickcount = parseInt(localStorage.getItem(path));

    localStorage.setItem(path, clickcount + 1)
  } else {
    localStorage.setItem(path, 1)
  }
}

function viewResults() {
  const keys = Object.keys(localStorage)
  const liArray = keys.map(function(el){
    return `<li>You visited ${el} ${localStorage.getItem(el)} time(s)</li>`
  })
	let button = document.getElementsByClassName('btn')[0];
  button.insertAdjacentHTML('afterend', `<ul>${liArray.join('')}</ul>`);
  localStorage.clear()
}
