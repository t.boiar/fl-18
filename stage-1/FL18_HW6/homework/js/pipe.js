const pipe = (value, ...funcs) => {
  let arg = value;
  for (let func of funcs){
    try {
      arg = func(arg);
    } catch (e) {
      arg = `Provided argument at position ${funcs.indexOf(func)} is not a function!`
      break;
    }
  }
  return arg;
};
