function isEquals(firstArg, secondArg) {
  return firstArg === secondArg;
}
// console.log(isEquals(3, 3));

function isBigger(firstArg, secondArg) {
  return firstArg > secondArg;
}
// console.log(isBigger(5, -1));

function storeNames(...strings) {
  return strings;
}
// console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'));

function getDifference(firstArg, secondArg) {
  let result = 0;
  if (firstArg > secondArg) {
    result = firstArg - secondArg;
  } else if (firstArg < secondArg) {
    result = secondArg - firstArg;
  }
  return result;
}
// console.log(getDifference(5, 3));
// console.log(getDifference(5, 8));
// console.log(getDifference(8, 8));

// function negativeCount(arr) {
//   let counter = 0;
//   for (let el of arr) {
//     if(el < 0){
//       counter++;
//     }
//   }
//   return counter;
// }

function negativeCount(arr) {
  return arr.filter((el) => el < 0).length
}
// console.log(negativeCount([0, -3, -5, -7]));
// console.log(negativeCount([4, 3, 2, 9]));

function letterCount(str, match) {
  let counter = 0;
  let lowStr = str.toLowerCase();
  let lowMatch = match.toLowerCase();

  for(let el of lowStr){
    if(el === lowMatch){
      counter++;
    }
  }
  return counter;
}
// console.log(letterCount("Marry", "r"));
// console.log(letterCount("Barny", "y"));
// console.log(letterCount("", "z"));

// function countPoints(scores) {
//   return scores.reduce((points, score) => {
//     const [x, y] = score.split(':').map((i) => parseFloat(i));
//     const maxPoint = 3;
//
//     if(x > y) {
//       points += maxPoint;
//     } else if (x < y) {
//       points += 0;
//     } else if (x === y) {
//       points += 1;
//     }
//
//     return points;
//   }, 0)
// }
// console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']));

function countPoints(scores) {
  let points = 0;
  const maxPoint = 3;
  for (let el of scores) {
    const x = parseInt(el.split(':')[0]);
    const y = parseInt(el.split(':')[1]);

    if(x > y) {
      points += maxPoint;
    } else if (x < y) {
      continue;
    } else {
      points += 1;
    }

  }
  return points;
}
// console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']));
