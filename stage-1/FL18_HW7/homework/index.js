function getAge(birthday) {
  let dateDiffer = Date.now() - birthday.getTime();
  let ageDate = new Date(dateDiffer);
  const newEpoch = 1970;
  return Math.abs(ageDate.getUTCFullYear() - newEpoch);
}

function getWeekDay(date) {
  let options = {weekday: 'long'};
  let weekday = new Intl.DateTimeFormat('en-US', options).format(date)
  return weekday;
}

function getAmountDaysToNewYear(){
  let today = new Date();
  const newYear = new Date(today.getFullYear(), 0, 1);
  newYear.setFullYear(newYear.getFullYear() + 1);

  const millisecInSec = 1000;
  const secInMin = 60;
  const minInHour = 60;
  const hoursInDay = 24;
  const oneDay= millisecInSec * secInMin * minInHour * hoursInDay;

  return Math.ceil((newYear.getTime()-today.getTime())/oneDay)
}

function getProgrammersDay(year) {
  const firstYearDay = new Date(year, 0, 1);
  const toProgramDay = 255;
  firstYearDay.setDate(firstYearDay.getDate() + toProgramDay);

  const weekday = getWeekDay(firstYearDay);
  let programDateArr = firstYearDay.toDateString().split(' ');

  const dayIndex = 2;
  const yearIndex = 3;
  const monthIndex = 1;

  return `${programDateArr[dayIndex]} ${programDateArr[monthIndex]}, ${programDateArr[yearIndex]} (${weekday})`
}

function howFarIs(specifiedWeekday) {
  const weekday = getWeekDay(new Date());
  const week = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
  specifiedWeekday = specifiedWeekday[0].toUpperCase() + specifiedWeekday.slice(1);

  if(specifiedWeekday === weekday) {
    return `Hey, today is ${ specifiedWeekday } =)`
  }
  let indexOfSpecifDay = week.indexOf(specifiedWeekday);
  let indexOfDayToday = week.indexOf(weekday);

  let number = 0;
  if(indexOfDayToday < indexOfSpecifDay) {
    number = indexOfSpecifDay - indexOfDayToday;
  } else {
    number = indexOfSpecifDay + week.length - indexOfDayToday;
  }
  return `It's ${ number } day(s) left till ${ specifiedWeekday }`
}

function isValidIdentifier(string) {
  const re = /^[a-zA-Z_$][0-9a-zA-Z_$]*$/
  return re.test(string);
}

function capitalize(str) {
  const capitalStr = str.replace(/\b\w/g, letter => letter.toUpperCase());

  return capitalStr;
 }

function isValidAudioFile(audioFileName) {
  const re = /^([^0-9_$][a-zA-Z]{0,})\.(mp3|flac|alac|aac)$/g
  return re.test(audioFileName)
}

function getHexadecimalColors(str) {
  const re = /#([a-f0-9]{3}){1,2}\b/gi;
  const arrWithColors = str.match(re);
  return arrWithColors || []
}

function isValidPassword(password) {
  const re = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,}$/
  return re.test(password)
}

function addThousandsSeparators(numbOrStr) {
  const re = /\B(?=(\d{3})+(?!\d))/g;
  return numbOrStr.toString().replace(re, ',');
}

function getAllUrlsFromText(text) {
  const re = /(https?:\/\/[^\s]+)/g;
  const arrWithURL = text.match(re)
  return arrWithURL || []
}
