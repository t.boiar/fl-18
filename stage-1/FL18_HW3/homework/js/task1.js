let error = false;
const initialAmount = parseFloat(prompt("Initial amount:"));
if (isNaN(initialAmount) || initialAmount < 1000) {
  alert("Invalid input data");
  error = true;
}

let years = parseInt(prompt("Number of years:"));
if (isNaN(years) || years < 1) {
  alert("Invalid input data");
  error = true;
}

let percentage = parseFloat(prompt("Percentage of a year:"));
if (isNaN(years) || percentage > 100) {
  alert("Invalid input data");
  error = true;
}
if (!error) {
  let calcProfit = 0;
  let calcTotalAmount = initialAmount;
  for(let i = 0; i < years; i++) {
    calcProfit = calcTotalAmount/100 * percentage;
    calcTotalAmount += calcProfit;
  }
  const totalProfit = calcTotalAmount - initialAmount

  alert(`Initial amount ${initialAmount} \nNumber of years ${years}
    \nPercentage of year ${percentage} \nTotal profit: ${totalProfit.toFixed(2)}
    \nTotal amount: ${calcTotalAmount.toFixed(2)}`)
}
