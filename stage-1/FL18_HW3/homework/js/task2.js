let playGame = confirm("Do you want to play a game?")

if (!playGame) {
  alert("You did not become a billionaire, but can.")
}

let range = 8;
let totalPrize = 0;
let defaultPrize = 100;

while(playGame){
  let random = Math.floor(Math.random() * (range - 0 + 1) + 0);
  let message = "Do you want to play again?"
  let notification = "Thank you for your participation."

  for(let attempts = 0; attempts < 3; attempts++) {
    let prize = defaultPrize;
    if(attempts === 1) {
      prize /= 2;
    } else if (attempts === 2) {
      prize /= 4;
    }
    let chooseNumber = parseInt(prompt(`Choose a roulette pocket number from 0 to ${range}.
    \nAttempts left: ${3 - attempts}. \nTotal prize: ${totalPrize}$. \nPossible prize on current attempt: ${prize}$`));


    if (chooseNumber === random) {
      notification = "Congratulation, you won!";
      message = "Do you want to continue?";
      defaultPrize *= 2;
      range += 4;
      totalPrize += prize;
      break;
    }

  }
  alert(`${notification} Your prize is: ${totalPrize}$`);
  playGame = confirm(message);
}
