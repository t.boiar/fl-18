/* START TASK 1: Your code goes here */
const allCells = document.getElementsByTagName('td');

function changeColor(eventObj) {
  switch(eventObj.target.id) {
    case '5':
    if(eventObj.target.offsetParent.style.backgroundColor){
      eventObj.target.offsetParent.style.backgroundColor = null;
    } else {
      eventObj.target.offsetParent.style.backgroundColor = 'green';
    }
    break;
    case '0':
    case '3':
    case '6':
    if(eventObj.target.parentElement.style.backgroundColor){
      eventObj.target.parentElement.style.backgroundColor = null;
    } else {
    eventObj.target.parentElement.style.backgroundColor = 'blue';
  }
    break;
    default:
    if(eventObj.target.style.backgroundColor){
      eventObj.target.style.backgroundColor = null;
    } else {
      eventObj.target.style.backgroundColor = 'yellow';
  }
    break;
  }
}
for (let cell of allCells) {
  cell.addEventListener('click', changeColor);
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
const message = document.getElementById('message');
const inputTel = document.getElementById('input-tel');
const button = document.getElementById('button');

function isValid(value) {
  value = inputTel.value;
  message.innerHTML = '<span class="message-invalid">Type number does not follow format <br>+380*********</span>'
  inputTel.style.borderColor = '#F08080';
  button.disabled = true;

  const reg = /[+380]{4}[0-9]{9}/
  if (reg.test(value)){
  message.innerHTML = '';
  inputTel.style.borderColor = '#000';
  button.disabled = false;
 }
}
inputTel.addEventListener('input', isValid);

function sendNumber() {
  message.innerHTML = '<span class="message-sent">Data was successfully sent</span>'
}
button.addEventListener('click', sendNumber);
/* END TASK 2 */

/* START TASK 3: Your code goes here */
const ball = document.getElementById('ball');
const court = document.getElementById('court');

const teamA = document.getElementById('team-A');
const teamB = document.getElementById('team-B');
const scoreNotification = document.getElementById('score-notification');

let scoreToA = 0;
let scoreToB = 0;

const xMinA = 30;
const xMaxA = 45;

const xMinB = 550;
const xMaxB = 565;

const yMin = 157;
const yMax = 173;

let timer;
function moveBallbyClick(eventObj) {
  const ballRadius = 20;
  const xClick = eventObj.offsetX;
  const yClick = eventObj.offsetY;

  const moveX = xClick - ballRadius;
  const moveY = yClick - ballRadius;
  ball.style.left = `${moveX}px`;
  ball.style.top = `${moveY}px`;

  if(yMin < yClick && yClick < yMax){
    const timerDelay = 3000;
    if(xMinA < xClick && xClick < xMaxA){
      scoreToB += 1
      teamB.innerHTML = `Team B:${scoreToB}`;
      scoreNotification.innerHTML = '<span class="notification-teamB">Team B score!</span>';
      if(scoreNotification){
        clearTimeout(timer);
      }
      timer = setTimeout(() => {
        scoreNotification.innerHTML = ''
      }, timerDelay);

    } else if(xMinB < xClick && xClick < xMaxB){
      scoreToA += 1
      teamA.innerHTML = `Team A:${scoreToA}`;
      scoreNotification.innerHTML = '<span class="notification-teamA">Team A score!</span>';
      if(scoreNotification){
        clearTimeout(timer);
      }
      timer = setTimeout(() => {
        scoreNotification.innerHTML = ''
      }, timerDelay);
  }
}
}
court.addEventListener('click', moveBallbyClick)
/* END TASK 3 */
