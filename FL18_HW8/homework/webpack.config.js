const HtmlWebpackPlugin = require('html-webpack-plugin');
const Minicss = require('mini-css-extract-plugin');

require('babel-polyfill');

module.exports = {

  target: ['web', 'es5'],
  entry: ['babel-polyfill', `${__dirname}/js/variables.js`, `${__dirname}/js/computer.js`, `${__dirname}/js/game.js`],
  output: {
    path: `${__dirname}/dist`,
    filename: 'app.js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          Minicss.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpe?g|webp|git|svg|)$/i,
        use: [
          {
            loader: 'img-optimize-loader',
          },
        ],
      },

    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: 'body',
    }),
    new Minicss({
      filename: 'style.css',
    }),
  ],
  devServer: {
    contentBase: './',
    port: 8000,
  },
};
