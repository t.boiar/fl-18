import '../scss/style.scss';

const rock = document.getElementById('rock');
const paper = document.getElementById('paper');
const scissors = document.getElementById('scissors');
const computerOptions = ['Rock', 'Paper', 'Scissors'];
const reset = document.getElementById('reset');
const result = document.getElementById('result');
const finalResult = document.getElementById('final-result');

export {
  rock, paper, scissors, computerOptions, reset, result, finalResult,
};
