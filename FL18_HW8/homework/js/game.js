import {
  rock, paper, scissors, result, finalResult, reset,
} from './variables';
import computerChoiceProcess from './computer';

let playerScore = 0;
let computerScore = 0;
let round = 0;

const clearScore = () => {
  playerScore = 0;
  computerScore = 0;
  round = 0;
};

const resetResult = () => {
  result.innerHTML = '<span></span>';
  finalResult.innerHTML = '<span></span>';
  clearScore();
};
reset.addEventListener('click', resetResult);

const showResult = (playerChoice) => {
  if (playerScore === 3 || computerScore === 3) {
    resetResult();
  }
  const computerChoice = computerChoiceProcess();
  let winner = '';
  if (playerChoice === computerChoice) {
    winner = 'Tie';
  } else if (playerChoice === 'Rock') {
    if (computerChoice === 'Paper') {
      winner = 'You’ve LOST';
      computerScore += 1;
    } else {
      winner = 'You’ve WON!';
      playerScore += 1;
    }
  } else if (playerChoice === 'Scissors') {
    if (computerChoice === 'Rock') {
      winner = 'You’ve LOST';
      computerScore += 1;
    } else {
      winner = 'You’ve WON!';
      playerScore += 1;
    }
  } else if (playerChoice === 'Paper') {
    if (computerChoice === 'Scissors') {
      winner = 'You’ve LOST';
      computerScore += 1;
    } else {
      winner = 'You’ve WON!';
      playerScore += 1;
    }
  }

  round += 1;
  result.innerHTML = `<span>Round ${round}, ${playerChoice} vs. ${computerChoice}, ${winner}</span>`;

  if (playerScore === 3) {
    finalResult.innerHTML = '<span>Congrats! You’ve WON THE GAME!</span>';
  } else if (computerScore === 3) {
    finalResult.innerHTML = '<span>It\'s a pity! You’ve LOST!</span>';
  }
};
const rockChosen = () => showResult('Rock');
const paperChosen = () => showResult('Paper');
const scissorsChosen = () => showResult('Scissors');

rock.addEventListener('click', rockChosen);
paper.addEventListener('click', paperChosen);
scissors.addEventListener('click', scissorsChosen);
