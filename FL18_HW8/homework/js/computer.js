import { computerOptions } from './variables';

const computerChoiceProcess = () => {
  const choiceNumber = Math.floor(Math.random() * 3);
  const computerChoice = computerOptions[choiceNumber];
  return computerChoice;
};

export default computerChoiceProcess;
