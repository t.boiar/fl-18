// Write your code here
let resultIs = '';

$(document).ready(function() {
function handleOperator(equation, operator) {
	let lastValue = equation[equation.length - 1];
	const operators = ['/', '*', '+', '-'];
	if(operators.indexOf(lastValue) < 0) {
		return equation + operator;
	} else {
		return equation.substr(0, equation.indexOf(lastValue)) + operator;
	}
}
	function changeFirstOperator(equation, number) {
		if(equation === '/' || equation === '*') {
			return number;
		} else {
			return equation + number;
		}
	}

	function checkResult() {
		if (resultIs) {
			$('input').val('').css('color', 'black');
			resultIs = '';
		}
	}

	$('#0').click(function() {
		checkResult();
		$('input').val(changeFirstOperator($('input').val() , '0'));
	});

	$('#1').click(function() {
		checkResult();
		$('input').val(changeFirstOperator($('input').val() , '1'));
	});

	$('#2').click(function() {
		checkResult();
		$('input').val(changeFirstOperator($('input').val() , '2'));
	});

	$('#3').click(function() {
		checkResult();
		$('input').val(changeFirstOperator($('input').val() , '3'));
	});

	$('#4').click(function() {
		checkResult();
		$('input').val(changeFirstOperator($('input').val() , '4'));
	});

	$('#5').click(function() {
		checkResult();
		$('input').val(changeFirstOperator($('input').val() , '5'));
	});

	$('#6').click(function() {
		checkResult();
		$('input').val(changeFirstOperator($('input').val() , '6'));
	});

	$('#7').click(function() {
		checkResult();
		$('input').val(changeFirstOperator($('input').val() , '7'));
	});

	$('#8').click(function() {
		checkResult();
		$('input').val(changeFirstOperator($('input').val() , '8'));
	});

	$('#9').click(function() {
		checkResult();
		$('input').val(changeFirstOperator($('input').val() , '9'));
	});

	$('#divide').click(function() {
		checkResult();
		$('input').val(handleOperator($('input').val() , '/'));
	});
	$('#multiply').click(function() {
		checkResult();
		$('input').val(handleOperator($('input').val() , '*'));
	});
	$('#add').click(function() {
		checkResult();
		$('input').val(handleOperator($('input').val() , '+'));
	});

	$('#substract').click(function() {
		checkResult();
		$('input').val(handleOperator($('input').val() , '-'));
	});

	$('#equals').click(function() {

		let equation = $('input').val();
		if( equation.substr(equation.indexOf('/')+1, equation.length) === '0') {
			$('input').val('ERROR').css('color', 'red');
		} else {
			let total = Function('return ' + $('input').val());
			$('.calculator__log').prepend(`<p class="log__item">
			<span class="log__circle"></span><span class="equation">${equation} = ${total()}</span>
			<span class="log__cross">&#x2715;</span></p>`);
			
			$('.equation:contains('+ 48 +')').addClass('text-decoration');
			
			$('input').val(total);
		}
		resultIs = true;
	});

	$('.calculator__log').scroll(function() {
		let scrollTop = $('.calculator__log').scrollTop();
		console.log('Scroll Top:' + scrollTop);
	})

	$('#clear').click(function() {
		$('input').val('');
	});
	
	$('.calculator__log').on('click', '.log__circle', function(event){
		$(event.target).toggleClass('red');
	});

	$('.calculator__log').on('click', '.log__cross', function(event){
		$(event.target.parentElement).remove();
	console.log(event.target.parentElement);
		
	});

});



	


  