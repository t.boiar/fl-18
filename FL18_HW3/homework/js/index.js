'use strict';

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */
function Pizza(size, type) { 
	this.size = size;
	this.type = type;
	this.sizes = {
		'S': 50,
		'M': 75,
		'L': 100
	};
	this.types = {
		'VEGGIE': 50,
		'MARGHERITA': 60,
		'PEPPERONI': 70
	};
	this.extraIngredients = {
		'TOMATOES': 5,
		'CHEESE': 7,
		'MEAT': 9
	};
	this.addedExtraIngredients = [];
	if(arguments.length < 2) {
		throw new PizzaException(`Required two arguments, given: ${arguments.length}`)
	}
	if(!this.sizes.hasOwnProperty(this.size) || !this.types.hasOwnProperty(this.type)) {
		throw new PizzaException(`Invalid type`)
	}
 }

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = 'S';
Pizza.SIZE_M = 'M';
Pizza.SIZE_L = 'L';

Pizza.TYPE_VEGGIE = 'VEGGIE';
Pizza.TYPE_MARGHERITA = 'MARGHERITA';
Pizza.TYPE_PEPPERONI = 'PEPPERONI';

Pizza.EXTRA_TOMATOES = 'TOMATOES';
Pizza.EXTRA_CHEESE = 'CHEESE';
Pizza.EXTRA_MEAT = 'MEAT';

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI];
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT]

Pizza.prototype.addExtraIngredient = function(ingredient) {
	if(arguments.length > 1) {
		throw new PizzaException(`Required one arguments, given: ${arguments.length}`)
	}
	if(!this.extraIngredients.hasOwnProperty(ingredient)) {
		throw new PizzaException(`Invalid ingredient`)
	}
	const index = this.addedExtraIngredients.indexOf(ingredient);
	if(index < 0) {
		this.addedExtraIngredients.push(ingredient);
	} else { 
		throw new PizzaException(`Duplicate ingredient`);
	}
}

Pizza.prototype.removeExtraIngredient = function(ingredient) {
	if(arguments.length > 1) {
		throw new PizzaException(`Required one arguments, given: ${arguments.length}`)
	}
	if(!this.extraIngredients.hasOwnProperty(ingredient)) {
		throw new PizzaException(`Invalid ingredient`)
	}
	const index = this.addedExtraIngredients.indexOf(ingredient);
	if (index < 0) {
		throw new PizzaException(`Ingredient is not added`);
		} else {
			this.addedExtraIngredients.splice(index, 1);
	}
}

Pizza.prototype.getSize = function() {
	return this.size;
}

Pizza.prototype.getPrice = function() {
	let totalPrice = this.sizes[this.size] + this.types[this.type];
	for (const i of this.addedExtraIngredients) {
		totalPrice += this.extraIngredients[i];
	}
	return totalPrice;
}

Pizza.prototype.getPizzaInfo = function() {
	return `Size: ${this.size}, type: ${this.type};
	extra ingredients: ${this.addedExtraIngredients.join(', ')}; price: ${this.getPrice()}UAH.`
}

/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */
function PizzaException(log) {
	this.log = log;
}

PizzaException.prototype = Error.prototype;



/* It should work */ 
// small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// // console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_CORN);  // => Invalid ingredient